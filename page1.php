<!DOCTYPE html>
<html>
<head>
    <title>CreateWebSite</title>
    <meta charset="utf-8">
    <style>
        body{
            background-image: url(/proekt/images/pexels-photo.jpg);
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
        }
        input[type="submit"]{
            color: white;
            background-color: #8abff2;
            border-radius: 10px;
            border:2px solid #ccc;
            padding:10px 20px;
            text-transform: uppercase;    
        }
        .h1{
            margin-top: 25%;
            color: white;
        }
        .button a{
            text-decoration: none;
        }
    </style>
</head>
<body>
    <center>
        
        <div class="h1">    
        <h3>Креирајте страна! Се што треба е да пополните една форма!</h><br>
        </div>
        
        <div class="button">
            <a href="page2.php" ><input type="submit" value="ПОЧНЕТЕ"></a>
        </div>
    
    </center>
</body>
</html>