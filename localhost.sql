-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 22, 2018 at 11:44 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Proekt`
--
CREATE DATABASE IF NOT EXISTS `Proekt` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `Proekt`;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `cover_slika` varchar(255) DEFAULT NULL,
  `naslov` varchar(255) DEFAULT NULL,
  `podnaslov` varchar(255) DEFAULT NULL,
  `zavas` text,
  `telefon` varchar(30) DEFAULT NULL,
  `lokacija` varchar(60) DEFAULT NULL,
  `service` text,
  `url_img1` varchar(255) DEFAULT NULL,
  `opis_img1` text,
  `url_img2` varchar(255) DEFAULT NULL,
  `opis_img2` text,
  `url_img3` varchar(255) DEFAULT NULL,
  `opis_img3` text,
  `za_vas` text,
  `linkedin` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `google` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `cover_slika`, `naslov`, `podnaslov`, `zavas`, `telefon`, `lokacija`, `service`, `url_img1`, `opis_img1`, `url_img2`, `opis_img2`, `url_img3`, `opis_img3`, `za_vas`, `linkedin`, `facebook`, `twitter`, `google`) VALUES
(1, 'https://static.pexels.com/photos/221471/pexels-photo-221471.jpeg', 'Beach', 'Sunny', 'Sunny Beach Bar', '0098778123123', 'Greece', 'produkti', 'https://static.pexels.com/photos/88212/pexels-photo-88212.jpeg', 'asdfghjkl', 'https://static.pexels.com/photos/88212/pexels-photo-88212.jpeg', 'asdfghjkl', 'https://static.pexels.com/photos/88212/pexels-photo-88212.jpeg', 'asdfghjkl', 'Best beach bar', 'linkedin.com', 'facebook.com', 'twitter.com', 'plus.google.com'),
(2, 'https://static.pexels.com/photos/221471/pexels-photo-221471.jpeg', 'Beach', 'Sunny', 'Sunny Beach Bar', '0098778123123', 'Greece', 'produkti', 'https://static.pexels.com/photos/88212/pexels-photo-88212.jpeg', 'asdfghjkl', 'https://static.pexels.com/photos/88212/pexels-photo-88212.jpeg', 'asdfghjkl', 'https://static.pexels.com/photos/88212/pexels-photo-88212.jpeg', 'asdfghjkl', 'Best beach bar', 'linkedin.com', 'facebook.com', 'twitter.com', 'plus.google.com'),
(3, 'https://static.pexels.com/photos/221471/pexels-photo-221471.jpeg', 'Beach', 'Sunny', 'Sunny Beach Bar', '0098778123123', 'Greece', 'produkti', 'https://static.pexels.com/photos/88212/pexels-photo-88212.jpeg', 'asdfghjkl', 'https://static.pexels.com/photos/88212/pexels-photo-88212.jpeg', 'asdfghjkl', 'https://static.pexels.com/photos/88212/pexels-photo-88212.jpeg', 'asdfghjkl', 'Best beach bar', 'linkedin.com', 'facebook.com', 'twitter.com', 'plus.google.com'),
(4, 'https://static.pexels.com/photos/221471/pexels-photo-221471.jpeg', 'Beach', 'Sunny', 'Sunny Beach Bar', '0098778123123', 'Greece', 'produkti', 'https://static.pexels.com/photos/88212/pexels-photo-88212.jpeg', 'asdfghjkl', 'https://static.pexels.com/photos/88212/pexels-photo-88212.jpeg', 'asdfghjkl', 'https://static.pexels.com/photos/88212/pexels-photo-88212.jpeg', 'asdfghjkl', 'Best beach bar', 'linkedin.com', 'facebook.com', 'twitter.com', 'plus.google.com'),
(5, 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best bar in the world', '0098798123123', 'Greece', 'produkti', 'https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/flip.jpg', 'Sad dog', 'https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/flip.jpg', 'Sad dog', 'https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/flip.jpg', 'Sad dog', 'Free first drink for all', 'https://www.linkedin.com/', 'https://www.facebook.com/', 'https://twitter.com/?lang=en', 'https://plus.google.com/discover'),
(6, 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best bar in the world', '0098798123123', 'Greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Sad dog', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Sad dog', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Sad dog', 'Free first drink for all', 'https://www.linkedin.com/', 'https://www.facebook.com/', 'https://twitter.com/?lang=en', 'https://plus.google.com/discover'),
(7, 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best bar in the world', '0098798123123', 'Greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Sad dog', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Sad dog', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Sad dog', 'Free first drink for all', 'https://www.linkedin.com/', 'https://www.facebook.com/', 'https://twitter.com/?lang=en', 'https://plus.google.com/discover'),
(8, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(9, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(10, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(11, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(12, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(13, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(14, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(15, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(16, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(17, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(18, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(19, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(20, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(21, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(22, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(23, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(24, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(25, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(26, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(27, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(28, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(29, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(30, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(31, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Best beach bar', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'asd', 'dsa', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(32, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(33, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png'),
(34, 'https://wallpaperbrowse.com/media/images/pexels-photo-248797.jpeg', 'Bar', 'Sunny Beach', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', '0098789123123', 'greece', 'produkti', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', 'https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', 'https://imageog.flaticon.com/icons/png/512/34/34227.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF', 'https://image.freepik.com/free-icon/facebook-logo-with-rounded-corners_318-9850.jpg', 'https://image.freepik.com/free-icon/twitter-logo_318-40459.jpg', 'https://seeklogo.com/images/G/google-plus-icon-logo-9A1F58888D-seeklogo.com.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;--
-- Database: `cinema`
--
CREATE DATABASE IF NOT EXISTS `cinema` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cinema`;

-- --------------------------------------------------------

--
-- Table structure for table `movie`
--

CREATE TABLE `movie` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `kod` varchar(50) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `zhanr_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movie`
--

INSERT INTO `movie` (`id`, `name`, `kod`, `date`, `zhanr_id`) VALUES
(1, 'Hangover', '123', 17112017, NULL),
(2, 'Transformers', '124', 18112017, NULL),
(3, 'Furious7', '125', 19112017, NULL),
(4, 'Hangover 2', '126', 20112017, NULL),
(5, 'Hangover 3', '127', 21112017, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `zhanr`
--

CREATE TABLE `zhanr` (
  `id` int(11) NOT NULL,
  `zhanr` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zhanr`
--

INSERT INTO `zhanr` (`id`, `zhanr`) VALUES
(18, '1'),
(19, '2'),
(20, '2'),
(21, '1'),
(22, '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zhanr_id` (`zhanr_id`);

--
-- Indexes for table `zhanr`
--
ALTER TABLE `zhanr`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `movie`
--
ALTER TABLE `movie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `zhanr`
--
ALTER TABLE `zhanr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `movie`
--
ALTER TABLE `movie`
  ADD CONSTRAINT `movie_ibfk_1` FOREIGN KEY (`zhanr_id`) REFERENCES `zhanr` (`id`),
  ADD CONSTRAINT `movie_ibfk_2` FOREIGN KEY (`zhanr_id`) REFERENCES `zhanr` (`id`);
--
-- Database: `contact`
--
CREATE DATABASE IF NOT EXISTS `contact` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `contact`;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `name`) VALUES
(1, 'San Francisco'),
(2, 'London'),
(3, 'New York'),
(4, 'Paris');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `first_name`, `last_name`, `age`, `location`) VALUES
(1, 'Kristijan', 'Pecev', 25, '3'),
(2, 'Nina', 'Angjelkovska', 24, '4'),
(3, 'Dule', 'Veljanovski', 26, '2'),
(4, 'Vladimir', 'Gadzovski', 27, '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;--
-- Database: `kino`
--
CREATE DATABASE IF NOT EXISTS `kino` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `kino`;

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

CREATE TABLE `film` (
  `id` int(11) NOT NULL,
  `zhanr` varchar(255) NOT NULL,
  `kod` int(11) NOT NULL,
  `datum` date DEFAULT NULL,
  `zhanr_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proekcii`
--

CREATE TABLE `proekcii` (
  `id` int(11) NOT NULL,
  `datum` date DEFAULT NULL,
  `start` varchar(255) DEFAULT NULL,
  `kraj` varchar(255) DEFAULT NULL,
  `cena` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zhanr`
--

CREATE TABLE `zhanr` (
  `id` int(11) NOT NULL,
  `zhanr` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zhanr_id` (`zhanr_id`);

--
-- Indexes for table `proekcii`
--
ALTER TABLE `proekcii`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zhanr`
--
ALTER TABLE `zhanr`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `proekcii`
--
ALTER TABLE `proekcii`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `film`
--
ALTER TABLE `film`
  ADD CONSTRAINT `film_ibfk_1` FOREIGN KEY (`zhanr_id`) REFERENCES `zhanr` (`id`);
--
-- Database: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Table structure for table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Table structure for table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Table structure for table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Table structure for table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Table structure for table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Table structure for table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Table structure for table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

-- --------------------------------------------------------

--
-- Table structure for table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Table structure for table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Table structure for table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2017-10-13 15:51:34', '{"collation_connection":"utf8mb4_unicode_ci"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Table structure for table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indexes for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indexes for table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indexes for table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indexes for table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indexes for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indexes for table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indexes for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indexes for table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indexes for table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indexes for table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indexes for table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indexes for table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;--
-- Database: `student`
--
CREATE DATABASE IF NOT EXISTS `student` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `student`;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `salary` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `grade` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `address` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `first_name`, `last_name`, `date_of_birth`, `address`) VALUES
(3, 'Kristijan', 'Pecev', '1991-11-23', 'france preshern'),
(4, 'Nina', 'Angjelkovska', '1993-10-09', 'dautica'),
(5, 'Zoki', 'Pecev', '1963-11-26', 'france preshern');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `pages` int(11) DEFAULT NULL,
  `isbn` varchar(50) DEFAULT NULL,
  `publication_date` date DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `author_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `name`, `pages`, `isbn`, `publication_date`, `publisher`, `author_id`) VALUES
(3, 'Kristijan', 100, 'abc', '2000-10-10', 'Prosvetno delo', 3),
(4, 'Nina', 200, 'abc', '2000-10-10', 'Prosvetno delo', 4),
(5, 'Zoki', 150, 'abc', '2000-10-10', 'Prosvetno delo', 5);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `adress` text,
  `date_of_birth` date DEFAULT NULL,
  `telephone_number` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_id` (`author_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
