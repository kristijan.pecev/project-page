<?php

$username = 'root';
$password = 'root';
$database_host = 'localhost';
$database_name = 'Proekt';
$database_type = 'mysql';

try
{

	$connection = new PDO("$database_type:host=$database_host;dbname=$database_name", $username, $password);

	$statement = $connection->prepare('INSERT INTO page (cover_slika, naslov, podnaslov, zavas, telefon, lokacija, service, url_img1, opis_img1, url_img2, opis_img2, url_img3, opis_img3, za_vas, linkedin, facebook, twitter, google) VALUES (:cover_slika, :naslov, :podnaslov, :zavas, :telefon, :lokacija, :service, :url_img1, :opis_img1, :url_img2, :opis_img2, :url_img3, :opis_img3, :za_vas, :linkedin, :facebook, :twitter, :google)');

	$statement->bindParam(':cover_slika', $_POST['cover_slika']);
	$statement->bindParam(':naslov', $_POST['naslov']);
	$statement->bindParam(':podnaslov', $_POST['podnaslov']);
	$statement->bindParam(':zavas', $_POST['zavas']);
	$statement->bindParam(':telefon', $_POST['telefon']);
	$statement->bindParam(':lokacija', $_POST['lokacija']);
	$statement->bindParam(':service', $_POST['service']);
	$statement->bindParam(':url_img1', $_POST['url_img1']);
	$statement->bindParam(':opis_img1', $_POST['opis_img1']);
	$statement->bindParam(':url_img2', $_POST['url_img2']);
	$statement->bindParam(':opis_img2', $_POST['opis_img2']);
	$statement->bindParam(':url_img3', $_POST['url_img3']);
	$statement->bindParam(':opis_img3', $_POST['opis_img3']);
	$statement->bindParam(':za_vas', $_POST['za_vas']);
	$statement->bindParam(':linkedin', $_POST['linkedin']);
	$statement->bindParam(':facebook', $_POST['facebook']);
	$statement->bindParam(':twitter', $_POST['twitter']);
	$statement->bindParam(':google', $_POST['google']);


$result = $statement->execute();

$id = $connection->lastInsertId();

header('Location: page3.php?id='.$id);


}
catch(PDOException $e){
    var_dump($e);
}

?>