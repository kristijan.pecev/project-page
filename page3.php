<?php

$id = $_GET['id'];

$username = 'root';
$password = 'root';
$database_host = 'localhost';
$database_name = 'Proekt';
$database_type = 'mysql';

$connection = new PDO("$database_type:host=$database_host;dbname=$database_name", $username, $password);

$statement = $connection->prepare('SELECT * FROM page WHERE id = :id');

$statement->bindValue(':id', $id);

$statement->execute();

$result = $statement->fetch(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html>
<head>
    <title>CreateWebSite</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="page3.css">
</head>
<body>
    <div class="navi">
        <ul>
                <a href="#"><li>ДОМА</li></a>
                <a href="#about"><li>ЗА НАС</li></a>
                <a href="#servis"><li>СЕРВИСИ ИЛИ ПРОДУКТИ</li></a>
                <a href="#contact"><li>КОНТАКТ</li></a>
        </ul>
    </div>
    <div class="img" style="background-size: cover; background-image: url(' <?php echo $result['cover_slika']?>')">
        <center>
            <div class="h1">
                <h1><?php echo $result['naslov'];?></h1>
                <h5><?php echo $result['podnaslov'];?></h5>
            </div>
        </center>
    </div>
    <div id="about" class="about">
        <h2>За нас</h2>
        <p><?php echo $result['zavas'];?></p>
    </div>
    <div class="phone">
        <b><p><?php echo $result['telefon'];?></p></b>
        <p>Внесениот број</p><br>
        <b><p><?php echo $result['lokacija'];?></p></b>
        <p>Внесената локација</p>
    </div>
    <center>
        <div id="servis" class="servis">
            <b><h3><?php echo $result['service'];?></h3></b>
        </div>
    </center>
    <div class="empty">
        <div class="images">
            <img src="<?php echo $result['url_img1'];?>" width="100%">
        </div>
        <div class="images">
            <img src="<?php echo $result['url_img2'];?>" width="100%">
        </div>
        <div class="images">
            <img src="<?php echo $result['url_img3'];?>" width="100%">
        </div>
    </div>
    <div class="products">
        <h4>Опис за првиот продукт</h4>
        <p><?php echo $result['opis_img1'];?><p>    
    </div>
    <div class="products">
        <h4>Опис за вториот продукт</h4>
        <p><?php echo $result['opis_img2'];?><p>    
    </div>
    <div class="products">
        <h4>Опис за третиот продукт</h4>
        <p><?php echo $result['opis_img3'];?><p>
    </div>
    <center>
        <div id="contact" class="contact">
            <b><h3>Контакти</h3></b>
        </div>
    </center> 
    <div class="text">
        <b><p>Текст</p></b>
        <p><?php echo $result['za_vas'];?></p>
    </div>
    <center>
        <div class="form">
            <form>
                <b><label>Име</label></b><br>
                <input type="text" placeholder="Име" name="name" class="input"><br>
                <b><label>Емаил</label></b><br>
                <input type="email" placeholder="Емаил" name="email" class="input"><br>
                <b><label>Порака</label></b><br>
                <textarea name="Порака" rows="5" cols="86"></textarea><br>
                <input type="submit" value="Испрати">
            </form>
        </div>
    </center>
<div class="bar">
    <div class="text_bar">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco.</p>
    </div>
    <div class="logo">
        <img src="<?php echo $result['linkedin'];?>" alt="ova">
        <img src="<?php echo $result['facebook'];?>" alt="ova">
        <img src="<?php echo $result['twitter'];?>" alt="ova">
        <img src="<?php echo $result['google'];?>" alt="ova">
    </div>
</div>
</body>
</html>   